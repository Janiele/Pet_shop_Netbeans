
import br.com.ifma.petshop.animal.Animal;
import br.com.ifma.petshop.animal.Cão;
import br.com.ifma.petshop.animal.Gato;
import br.com.ifma.petshop.cliente.PessoaFisica;
import br.com.ifma.petshop.servico.Banho;
import br.com.ifma.petshop.servico.Banho_Tosa;
import br.com.ifma.petshop.servico.Servico;
import br.com.ifma.petshop.servico.Tosa;
import javax.swing.JOptionPane;



public class PetShop {

   
    public static void main(String[] args) {
        // TODO code application logic here
        PessoaFisica p = new PessoaFisica("Janiele", 60920232, 81635651);
        //polimorfismo no animal e serviço 
        Animal a;
        Integer op= Integer.valueOf(JOptionPane.showInputDialog("Que animal é o seu: \n 1: Gato \n 2: Cão "));
        switch(op){
            case 1:
                a = new Gato (10.2,20, "\nCor: preto", "\nNome: Amora", "\nRaça: puldou", 2, "\nSexo: fêmea");
                a.status();
                break;
            case 2:
                a= new Cão(12.2, 0.45, "\nNome: Kiwi", "\nRaça: Pastor Alemão", 1, "\nSexo: macho");
                a.status();
                break;
        }
        
        
        Servico s;
        Integer es = Integer.valueOf(JOptionPane.showInputDialog("Que serviço você deseja: \n 1: Banho \n 2: Tosa \n 3: Banho e Tosa "));
        switch(es){
            case 1:
                s = new Banho(JOptionPane.showInputDialog("Escolha uma temperatura: "
               + "\n 1: Fria \n 2: Morna"),(JOptionPane.showInputDialog("Qual modalidade deseja para seu animal:"
               + "\n 1: Simples \n 2: Completo")), 30.00, 001);
                s.info();
                break;
            case 2:
                s = new Tosa(JOptionPane.showInputDialog("Sua tosa desejada será: \n 1: Completa"
                        + "\n 2: Aparação"), 30.00, 011);
               s.info();
                break;
            case 3:
                s = new Banho_Tosa( JOptionPane.showInputDialog("Deseja serviço com massagem:\n1:Sim \n2:Não"), 50.00, 011);
                s.info();
                break;
 
        }
        
       
      
       
     
       
      
       
      
       
        
        
        
    }
    
}
