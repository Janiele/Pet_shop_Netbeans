
package br.com.ifma.petshop.servico;

import br.com.ifma.petshop.interfaces.InterfaceServico;
import javax.swing.JOptionPane;

public class Banho extends Servico implements InterfaceServico{
    private String temperatura; //temperatura da água 
    private String modalidade; //banho completo ou simples

    public Banho(String temperatura, String modalidade, double preco, int codigo) {
        super(preco, codigo);
        this.temperatura = temperatura;
        this.modalidade = modalidade;
    }

    public String getTemperatura() {
        return temperatura;
    }
    public void setTemperatura(String temperatura){
        
    }
    public void setTemperatura(int temperatura) {
        switch(temperatura){
            case 1:
                this.temperatura = "Fria";
                break;
            case 2:
                this.temperatura = "Morna";
                break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção Invalida!!!");
        } 
        
    }
    public String getModalidade() {
        return modalidade;
    }
    public void setModalidade(String modalidade){
        
    }
    public void setModalidade(int modalidade) {
        switch(modalidade){
            case 1:
                this.modalidade = "Simples";
                break;
                
            case 2:
                this.modalidade = "Completo";
                break;
                
                default:
                    JOptionPane.showMessageDialog(null, "Opção Invalida!!!");
        }
    }
    @Override
    public void info() {
        JOptionPane.showMessageDialog(null, "Você escolheu a opção banho." + this.temperatura + this.modalidade + super.getCodigo()+ super.getPreco());
        
        
    }
    
}
