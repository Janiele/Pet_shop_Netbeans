/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifma.petshop.servico;

/**
 *
 * @author Alunos
 */
public abstract class Servico {
    private double preco;
    private int codigo;
   

    public Servico(double preco, int codigo) {
        this.codigo = codigo;
       
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
    
    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public abstract void info();
}
