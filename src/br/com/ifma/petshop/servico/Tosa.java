
package br.com.ifma.petshop.servico;

import br.com.ifma.petshop.interfaces.InterfaceServico;
import javax.swing.JOptionPane;

public class Tosa extends Servico implements InterfaceServico{
    private String tipo_tosa; // tosa completa ou apararação

    public Tosa(String tipo_tosa, double preco, int codigo) {
        super(preco, codigo );
        this.tipo_tosa = tipo_tosa;
    }

    public String getTipo_tosa() {
        return tipo_tosa;
    }
    public void setTipo_tosa(String tipo_tosa){
        
    }
    public void setTipo_tosa(int tipo_tosa) {
        //this.tipo_tosa = tipo_tosa;
        switch(tipo_tosa){
            case 1:
                this.tipo_tosa = "Tosa completa";
                break;
            case 2:
                this.tipo_tosa = "Aparação";
                break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção Invalida!!!");
        }
        
    }

    @Override
    public void info() {
         JOptionPane.showMessageDialog(null, "Você escolheu a opção tosa." + this.tipo_tosa + super.getCodigo()+ super.getPreco());
    }
    
    
}
