/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifma.petshop.animal;

import br.com.ifma.petshop.interfaces.InterfaceAnimal;
import javax.swing.JOptionPane;

/**
 *
 * @author Alunos
 */
public class Cão extends Animal implements InterfaceAnimal{
    private double peso;
    private double altura;

    public Cão(double peso, double altura, String nome, String raca, int idade, String sexo) {
        super(nome, raca, idade, sexo);
        this.peso = peso;
        this.altura = altura;
    }
    public double getPeso() {
        return peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    public double getAltura() {
        return altura;
    }
    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public void status() {
        JOptionPane.showMessageDialog(null, "Seu animal escolhido foi um cão." + this.altura + this.peso + super.getNome() + super.getRaca() + super.getSexo());
    }
   
}
