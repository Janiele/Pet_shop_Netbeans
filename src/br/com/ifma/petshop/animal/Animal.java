/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifma.petshop.animal;

import javax.swing.JOptionPane;

/**
 *
 * @author Alunos
 */
public abstract class Animal {
    private String nome;
    private String raca;
    private int idade;
    private String sexo;

    public Animal(String nome, String raca, int idade, String sexo) {
        this.nome = nome;
        this.raca = raca;
        this.idade = idade;
        this.sexo = sexo;
    }
    
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getRaca() {
        return raca;
    }
    public void setRaca(String raca) {
        this.raca = raca;
    }
    public int getIdade() {
        return idade;
    }
    public void setIdade(int idade) {
        this.idade = idade;
    }
    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo){
        this.sexo = sexo;
    }
    public abstract void status();
    
    
}
