/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifma.petshop.animal;

import br.com.ifma.petshop.interfaces.InterfaceAnimal;
import javax.swing.JOptionPane;

/**
 *
 * @author Alunos
 */
public class Gato extends Animal implements InterfaceAnimal{
    private double peso;
    private double altura;
    private String cor;

    public Gato(double peso, double altura, String cor, String nome, String raca, int idade, String sexo) {
        super(nome, raca, idade, sexo);
        this.peso = peso;
        this.altura = altura;
        this.cor = cor;
    }
    
    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    @Override
    public void status() {
        JOptionPane.showMessageDialog(null, "Seu animal escolhido foi um gato." + this.cor + super.getNome() + super.getRaca() + super.getSexo());
    }
    
    
}
